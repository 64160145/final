/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.watsapon.oopfinal;

/**
 *
 * @author iHC
 */
public class Student {
    private String name;
    private String studentid;
    private String branch;
    private String sec;
    private String date;

    public Student(String name, String studentid, String branch, String sec, String date) {
        this.name = name;
        this.studentid = studentid;
        this.branch = branch;
        this.sec = sec;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSec() {
        return sec;
    }

    public void setSec(String sec) {
        this.sec = sec;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", studentid=" + studentid + ", branch=" + branch + ", sec=" + sec + ", date=" + date + '}';
    }
    
    
}
